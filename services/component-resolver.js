import Vue from 'vue'
import Post from '~/pages/posts/detail'
import Product from '~/pages/product'
import UrlComponents from '~/pages/urls'
import PageComponentPlugin from '~/vue-app/plugins/page-component'

const COMPONENT_MAP = {
    posts: Post,
    products: Product,
    urls: {
        ...UrlComponents
    }
  }

function sanitizeComponent (Component) {
    // If Component already sanitized
    if (Component.options && Component._Ctor === Component) {
      return Component
    }
    if (!Component.options) {
      Component = Vue.extend(Component) // fix issue #6
      Component._Ctor = Component
    } else {
      Component._Ctor = Component
      Component.extendOptions = Component.options
    }
    // For debugging purpose
    if (!Component.options.name && Component.options.__file) {
      Component.options.name = Component.options.__file
    }

    Component.use(PageComponentPlugin)

    return Component
  }

function resolveComponent(page) {
  try {
    const component = page.subtype 
      ? COMPONENT_MAP[page.type][page.subtype]
      : COMPONENT_MAP[page.type]

    return sanitizeComponent(component)
  } catch(e) {
    let message = page.subtype 
      ? `Unable to get component for page type '${page.type}' and subtype '${page.subtype}'`
      : `Unable to get component for page type '${page.type}'`

    throw message
  } 
}

export default resolveComponent