import Vue from 'vue'
import axios from 'axios'
import Router from 'vue-router'
import Login from '~/pages/login'
import resolveComponent from '~/services/component-resolver'

Vue.use(Router)

export async function createRouter(context) {
    const routes = await axios.get('http://api-skeleton.test/api/pages')
        .then((response) => {
            return response.data.map((page) => {
                return {
                    path: page.path,
                    component: resolveComponent(page),
                    meta: {
                        id: page.id
                    }
                }
            })
        })

    return new Router({
        mode: 'history',
        routes: [
            {
                name: 'login',
                path: '/login',
                component: Login
            },
            ...routes
        ]
        })
}