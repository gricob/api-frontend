const { resolve } = require('path')

module.exports = function() {
    this.addTemplate({
        fileName: 'index.js',
        src: require.resolve(__dirname + '/../vue-app/template', 'index.js')
    })
}