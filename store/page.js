export const state = () => ({
    current: null,
    menus: [],
    authenticated: false,
    auth_token: null
})

export const mutations = {
    set(state, page) {
        state.current = page
    },
    setMenus(state, menus) {
        state.menus = menus
    }
}

export const actions =  {
    set({commit}, page) {
        commit('set', page)
    },
    setMenus({commit}, menus) {
        commit('setMenus', menus)
    }
}