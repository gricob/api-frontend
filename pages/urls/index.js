import Home from '~/pages/urls/home'
import Blog from '~/pages/urls/blog'
import Store from '~/pages/urls/store'
import PrivateArea from '~/pages/urls/private-area'

export default {
    Home,
    Blog,
    Store,
    PrivateArea
}