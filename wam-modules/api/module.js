const path = require('path')

function apiModule(moduleOptions) {
  const defaultOptions = {
    extensions: [],
  }

  const options = {
    ...defaultOptions,
    ...(this.options.api || {})
  }

  options.extensions.forEach((extension) => {
    const file = 'api/' + extension + '.js'

    this.addTemplate({
      src: this.options.srcDir + '/' + file,
      fileName: file
    })
  })

  this.addPlugin({
    src: path.resolve(__dirname, 'plugin.js'),
    fileName: 'api.js',
    options
  })
}

module.exports = apiModule