<% options.extensions.forEach((ext) => {
    %>import <%= ext %> from './api/<%= ext %>'<%
}) %>

const apiMethods = {
    getResource(endpoint, params) {
        return this.get(endpoint, {params}).then((res) => res.data)
    },
    getMenus() {
        return this.getResource('/menus')
    },
    getPages() {
        return this.getResource('/pages')
    },
    getPage(id, params) {
        return this.getResource(`/pages/${id}`, params)
    }
}

const extendApiInstance = function (api) {
    let extendedMethods = {}
    Object.assign(extendedMethods, apiMethods)
    <% options.extensions.forEach((ext) => {%>
    extendedMethods = Object.assign(extendedMethods, <%= ext %>())
    <%}) %>

    for (let key in extendedMethods) {
        api[key] = extendedMethods[key].bind(api)
    }
}

const setupInterceptors = function (context) {
    context.$api.interceptors.request.use(function(config) {
        if (!context.$auth) {
            return config
        }

        const token = context.$auth.getToken('local')
        if (token) {
            config.headers.common['Authorization'] = token
        } else {
            context.$auth.logout()
        }

        return config
    })

    context.$api.interceptors.response.use(function(response) {
        switch (response.status) {
            case 401:
                context.$auth.logout()
                break;
            case 404:
                context.error()
        }

        return response
    }) 
}

export default function (context, inject) {
    // Create a custom axios instance
    const api = context.$axios.create({
      headers: {
        common: {
          Accept: 'application/json'
        }
      }
    })
    
    extendApiInstance(api)
    
    // Set baseURL to something different
    api.setBaseURL(context.env.API_BASE_URL)
  
    // Inject to context as $api
    context.$api = api
    inject('api', api)

    setupInterceptors(context, api)
  }