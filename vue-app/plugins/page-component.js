export default function install(Vue, options) {
    Vue.mixin({
        async fetch({$api, store, route}) {
            let page = await $api.getPage(route.meta[0].id, route.query)
            
            store.dispatch('page/set', page)
        }
    })
    

    Object.defineProperty(Vue.prototype, '$page', {
        get () { return this.$store.state.page.current }
    })

    Object.defineProperty(Vue.prototype, '$head', {
        get () { return this.$page.head }
    })

    Object.defineProperty(Vue.prototype, '$content', {
        get () { return this.$page.content }
    })
}