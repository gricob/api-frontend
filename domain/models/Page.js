import Model from '~/domain/models/model'

class PageHead extends Model {
    constructor(data) {
        super()
        
        this.title = data.title || ''
        this.meta = []
        this.links = []

        if (data.description) {
            this.addMeta('description', data.description)
        }

        if (data.alternates) {
            data.alternates.forEach(function(alternate) {
                this.addLink('alternate', alternate.hreflang, alternate.url)
            })
        }
        
    }

    addMeta(name, content) {
        this.meta.push({
            hid: name,
            name: name,
            content: content
        })
    }

    addLink(rel, hreflang, href) {
        this.links.push({rel, hreflang, href})
    }
}

class Page extends Model {
    constructor(data) {
        super()

        this.head = new PageHead(data.head)
        this.menus = data.menus || []
        this.breadcrumb = data.breadcrumb || []
        this.content = data.content
    }
}

export default Page