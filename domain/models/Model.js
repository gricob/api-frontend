class Model {
    toPOJO() {
        let pojo = {}
        Object.keys(this).forEach((property) => {
            pojo[property] = this[property] instanceof Model ? this[property].toPOJO() : this[property]
        })

        return pojo
    }
}

export default Model