export default function () {
    return {
        getPosts(params) {
            return this.getResource('/posts', params);
        }
    }
}