import Page from '~/domain/models/page'
import findRoute from '~/api/findRoute'
import resolveComponent from '~/services/component-resolver'

export default async function (store, to) {
  let route = await findRoute(to)
  if (route) {
      let component = resolveComponent(route.resource.type)

      store.dispatch('page/set', (new Page(route)).toPOJO())
  
      store.$router.addRoutes([{
          path: route.path,
          component: component
        }])
  }
}
