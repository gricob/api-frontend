import resolveComponent from '~/services/component-resolver'

export default async function (app) {
    console.log(app)
    let pages = await app.$api.getPages()

    if (!pages) {
        return
    }

    app.router.addRoutes(pages.map(function (page) {
        return {
            path: page.path,
            component: resolveComponent(page),
            meta: {
                id: page.id
            }
        }
    }))
}

