import Vue from 'vue'

import Container from '~/components/layout/container'
import WamPicture from '~/components/util/wam-picture'
import WamBaseMenu from '~/components/util/wam-base-menu'
import Breadcrumb from '~/components/util/breadcrumb'

Vue.component('container', Container)
Vue.component('breadcrumb', Breadcrumb)
Vue.component('wam-picture', WamPicture)
Vue.component('wam-base-menu', WamBaseMenu)