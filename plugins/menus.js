export default async function ({$api}, inject) {
    const menus = await $api.getMenus()

    inject('menus', menus)
}