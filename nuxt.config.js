import axios from 'axios'

export default () => {
  //const routesResponse = await axios.get('http://api-skeleton.test/api/routes')
  return {
    mode: 'universal',
    /*
    ** Headers of the page
    */
    head: {
      title: process.env.npm_package_name || '',
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
      ],
      link: [
        { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
      ]
    },
    /*
    ** Customize the progress-bar color
    */
    loading: { color: '#fff' },
    /*
    ** Global CSS
    */
    css: [
    ],
    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
      '~/plugins/global.js',
      '~/plugins/menus.js'
    ],
    /*
    ** Nuxt.js dev-modules
    */
    buildModules: [
      '@nuxtjs/router',
      '@nuxtjs/dotenv',
    ],
    /*
    ** Nuxt.js modules
    */
    modules: [
      '~/wam-modules/api/module.js',
      '@nuxtjs/axios',
      '@nuxtjs/auth',
    ],
    /*
    ** Build configuration
    */
    build: {
      /*
      ** You can extend webpack config here
      */
      extend (config, ctx) {
      }
    },
    generate: {
    },
    router: {
    },
    auth: {
      strategies: {
        local: {
          endpoints: {
            login: { url: 'http://api-skeleton.test/api/login_check', method: 'post', propertyName: 'token' },
            logout: { url: '/api/auth/logout', method: 'post' },
            user: false
          },
          tokenRequired: true,
          tokenType: 'bearer'
        }
      }
    },
    api: {
        extensions: ['news']
    }
  }
}
