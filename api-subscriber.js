var amqp = require('amqplib/callback_api');
var child_process = require('child_process')

var generationTimeout = null
function prepareGeneration() {
  if (generationTimeout) {
    clearTimeout(generationTimeout)
  }

  generationTimeout = setTimeout(() => {
    child_process.exec('npm run generate', (error, stdout, stderr) => {
      if (error) {
        console.error(error)
      } else {
        console.info('Static website generated successfully.')
      }
    })
  }, 5000)
}

amqp.connect('amqp://localhost', function(error0, connection) {
  if (error0) {
    throw error0;
  }

  connection.createChannel(function(error1, channel) {
    if (error1) {
      throw error1;
    }
    var queue = 'messages';

    channel.assertQueue(queue);

    channel.consume(queue, function(msg) {
        console.log(" [x] Received %s", msg.content.toString());

        prepareGeneration()

      }, {
          noAck: true
        });
  });
});